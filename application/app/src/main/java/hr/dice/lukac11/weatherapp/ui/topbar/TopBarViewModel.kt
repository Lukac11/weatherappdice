package hr.dice.lukac11.weatherapp.ui.topbar

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hr.dice.lukac11.weatherapp.data.LocationRepository
import hr.dice.lukac11.weatherapp.ui.state.CityNameState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.time.LocalDate

class TopBarViewModel(private val locationRepository: LocationRepository) : ViewModel() {
    private val _cityState = MutableStateFlow(CityNameState())
    val cityState: StateFlow<CityNameState> = _cityState
    private val _date = MutableStateFlow(LocalDate.MIN)
    val date: StateFlow<LocalDate> = _date
    fun getCityName() {
        viewModelScope.launch {
            try {
                locationRepository.getActiveLocation().collect() {
                    _cityState.value = CityNameState(it.cityName, isLoading = false)
                }
            } catch (e: Exception) {
                _cityState.value = CityNameState("", isLoading = false)
            }
        }
    }
    fun setDate(date: LocalDate) {
        _date.value = date
    }
}
