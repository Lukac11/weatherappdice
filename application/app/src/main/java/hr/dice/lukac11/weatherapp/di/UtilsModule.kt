package hr.dice.lukac11.weatherapp.di

import com.google.android.gms.location.FusedLocationProviderClient
import hr.dice.lukac11.weatherapp.database.PreferenceManager
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val utilsModule = module {
    single { FusedLocationProviderClient(androidApplication()) }
    single { PreferenceManager(get()) }
}
