package hr.dice.lukac11.weatherapp.ui.dailydetailsweather

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ramcosta.composedestinations.annotation.Destination
import hr.dice.lukac11.weatherapp.model.DailyDetailsCard
import hr.dice.lukac11.weatherapp.model.DailyDetailsItem
import hr.dice.lukac11.weatherapp.model.DailyDetailsTitle
import hr.dice.lukac11.weatherapp.model.DailyWeatherInfo
import org.koin.androidx.compose.getViewModel
import org.koin.core.parameter.parametersOf

/**
 * Screen shows Lazy Column with DailyDetailsCard, DailyDetailsTitle and DailyDetailsItem as items.
 * Gets data and shows it on screen.
 * @param dailyWeatherInfo is used for presenting all information from api to screen
 */
@Destination
@Composable
fun DailyDetailsWeatherScreen(dailyWeatherInfo: DailyWeatherInfo) {
    val viewModel: DailyDetailsWeatherViewModel = getViewModel() { parametersOf(dailyWeatherInfo) }

    val dailyDetailsWeatherComponentList by viewModel.dailyDetailsWeatherComponentList.collectAsState()

    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(horizontal = 14.dp)
            .padding(top = 70.dp)
    ) {
        for (component in dailyDetailsWeatherComponentList) {
            item {
                when (component) {
                    is DailyDetailsCard -> DailyDetailsWeatherCard(component)
                    is DailyDetailsTitle -> DailyDetailsWeatherSection(component)
                    is DailyDetailsItem -> DailyDetailsWeatherItem(component)
                }
            }
        }
    }
}

@Composable
@Preview
private fun PreviewDailyDetailsWeatherScreen() {
    DailyDetailsWeatherScreen(
        DailyWeatherInfo(
            "2022-12-15", 4.3, 31.7, 22.9,
            1671062400, 27.3, 1011.2, 81.2, "rain", "overcast", 11.2, 27.12, "8.11", "18.11", 112.01
        )
    )
}
