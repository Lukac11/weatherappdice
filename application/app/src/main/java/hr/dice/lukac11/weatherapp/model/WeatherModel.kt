package hr.dice.lukac11.weatherapp.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class WeatherModel(
    @SerialName("weather")
    val weather: List<Weather> = listOf(),
    @SerialName("main")
    val mainWeatherInfo: MainWeatherInfo? = null,
    @SerialName("visibility")
    val visibility: Int? = null,
    @SerialName("wind")
    val wind: Wind? = null,
    @SerialName("id")
    val id: Int? = null,
    @SerialName("name")
    val locationName: String? = null,
    @SerialName("cod")
    val cod: Int? = null
)
