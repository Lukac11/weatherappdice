package hr.dice.lukac11.weatherapp.navigation

import androidx.compose.animation.ExperimentalAnimationApi
import hr.dice.lukac11.weatherapp.R
import hr.dice.lukac11.weatherapp.ui.destinations.CurrentWeatherScreenDestination
import hr.dice.lukac11.weatherapp.ui.destinations.DailyWeatherScreenDestination
import hr.dice.lukac11.weatherapp.ui.destinations.DirectionDestination
import hr.dice.lukac11.weatherapp.ui.destinations.SettingsDestination
@OptIn(ExperimentalAnimationApi::class)
enum class BarItem(
    val icon: Int,
    val navRoute: DirectionDestination,
    val label: Int
) {

    CURRENT_WEATHER(
        icon = R.drawable.ic_baseline_date_range_24,
        navRoute = CurrentWeatherScreenDestination,
        label = R.string.today
    ),
    DAILY_WEATHER(
        icon = R.drawable.ic_baseline_calendar_today_24,
        navRoute = DailyWeatherScreenDestination,
        label = R.string.daily
    ),
    SETTINGS(
        icon = R.drawable.ic_baseline_settings_24,
        navRoute = SettingsDestination,
        label = R.string.settings
    )
}
