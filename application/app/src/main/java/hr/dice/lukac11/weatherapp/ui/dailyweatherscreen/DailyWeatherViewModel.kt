package hr.dice.lukac11.weatherapp.ui.dailyweatherscreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hr.dice.lukac11.weatherapp.data.DailyWeatherRepository
import hr.dice.lukac11.weatherapp.data.LocationRepository
import hr.dice.lukac11.weatherapp.data.UserSettingsRepository
import hr.dice.lukac11.weatherapp.enums.Days
import hr.dice.lukac11.weatherapp.enums.Unit
import hr.dice.lukac11.weatherapp.model.LocationModel
import hr.dice.lukac11.weatherapp.model.UserPreferences
import hr.dice.lukac11.weatherapp.ui.state.UiState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import okio.IOException

class DailyWeatherViewModel(private val dailyWeatherRepository: DailyWeatherRepository, private val locationRepository: LocationRepository, private val datastoreRepository: UserSettingsRepository) : ViewModel() {
    private val _uiState = MutableStateFlow<UiState>(UiState.Loading)
    val uiState: StateFlow<UiState> = _uiState
    private val _unitValue = MutableStateFlow<Unit>(Unit.STANDARD)
    val unitValue: StateFlow<Unit> = _unitValue
    private val _numberOfDays = MutableStateFlow(Days.THREE)
    val numberOfDays: StateFlow<Days> = _numberOfDays
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private var language: String = ""
    init {
        try {
            viewModelScope.launch {
                locationRepository.getActiveLocation()
                    .combine(datastoreRepository.getPreferenceFlow()) { location: LocationModel, userPreferences: UserPreferences ->
                        latitude = location.latitude
                        longitude = location.longitude
                        _unitValue.value = userPreferences.unit
                        language = userPreferences.language.visualCrossingWeatherApiValue
                        _numberOfDays.value = userPreferences.numberOfDays

                        val response = dailyWeatherRepository.getDailyWeatherData(
                            location.latitude,
                            location.longitude,
                            userPreferences.unit.visualCrossingWeatherApiValue,
                            userPreferences.unit.visualCrossingWeatherApiValue
                        )
                        _uiState.value = UiState.Success(data = response)
                    }
                    .collect()
            }
        } catch (ioException: IOException) {
            _uiState.value = UiState.InternetError
        } catch (e: Exception) {
            _uiState.value = UiState.Error
        }
    }
    fun loadCityWeatherData() {
        viewModelScope.launch() {
            try {
                val response = dailyWeatherRepository.getDailyWeatherData(
                    latitude,
                    longitude,
                    _unitValue.value.visualCrossingWeatherApiValue,
                    language
                )
                _uiState.value = UiState.Success(data = response)
            } catch (ioException: IOException) {
                _uiState.value = UiState.InternetError
            } catch (e: Exception) {
                _uiState.value = UiState.Error
            }
        }
    }
}
