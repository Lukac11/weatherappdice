package hr.dice.lukac11.weatherapp.model

import hr.dice.lukac11.weatherapp.enums.Days
import hr.dice.lukac11.weatherapp.enums.Language
import hr.dice.lukac11.weatherapp.enums.Theme
import hr.dice.lukac11.weatherapp.enums.Unit

data class UserPreferences(
    val theme: Theme,
    val language: Language,
    val unit: Unit,
    val numberOfDays: Days
)
