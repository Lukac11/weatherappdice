package hr.dice.lukac11.weatherapp.enums

enum class Unit(val openWeatherApiValue: String, val visualCrossingWeatherApiValue: String, val speed: String, val temperature: String, val pressure: String) {
    STANDARD("standard", "base", "km/h", "K", "hPa"),
    IMPERIAL("imperial", "us", "mph", "°F", "PSI"),
    METRIC("metric", "metric", "km/h", "°C", "hPa")
}
