package hr.dice.lukac11.weatherapp.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.SerialName
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.Locale
@Parcelize
@kotlinx.serialization.Serializable
data class DailyWeatherInfo(
    @SerialName("datetime")
    val date: String,
    @SerialName("windspeed")
    val windSpeed: Double,
    @SerialName("tempmax")
    val tempMax: Double,
    @SerialName("tempmin")
    val tempMin: Double,
    @SerialName("datetimeEpoch")
    val dateInMillis: Long,
    val temp: Double,
    val pressure: Double,
    val humidity: Double,
    val icon: String,
    val conditions: String,
    @SerialName("feelslikemax")
    val feelsLikeMax: Double,
    @SerialName("feelslikemin")
    val feelsLikeMin: Double,
    val sunrise: String,
    val sunset: String,
    @SerialName("winddir")
    val windDirection: Double,

) : Parcelable {
    val dayOfTheWeek: String
        get() =
            firstCharToUpperCase(LocalDate.parse(date).format(DateTimeFormatter.ofPattern(android.icu.text.DateFormat.WEEKDAY)))

    val dateFormatted: String
        get() = formatDate(date)

    val newWindDirection: String
        get() = getNewWindDirection(windDirection)

    val localDate: LocalDate
        get() = LocalDate.parse(date)
}

private fun formatDate(date: String): String {
    return LocalDate.parse(date).format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT))
}

private fun firstCharToUpperCase(text: String): String {
    return text.replaceFirstChar {
        if (it.isLowerCase()) it.titlecase(
            Locale.ROOT
        ) else it.toString()
    }
}

fun getNewWindDirection(windDir: Double): String {
    return when (windDir) {
        in 22.6..67.5 -> "NE"
        in 67.6..112.5 -> "E"
        in 112.6..157.5 -> "SE"
        in 157.6..202.5 -> "S"
        in 202.6..247.5 -> "SW"
        in 247.6..292.5 -> "W"
        in 292.6..337.5 -> "NW"
        in 337.6..359.9 -> "N"
        in 0.0..22.5 -> "N"
        else -> throw IllegalArgumentException("windDir must be in range 0 to 360")
    }
}
