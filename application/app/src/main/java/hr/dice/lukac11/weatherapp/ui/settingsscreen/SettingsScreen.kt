package hr.dice.lukac11.weatherapp.ui.settingsscreen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.ramcosta.composedestinations.annotation.Destination
import hr.dice.lukac11.weatherapp.R
import hr.dice.lukac11.weatherapp.data.UserSettingsRepository
import hr.dice.lukac11.weatherapp.database.PreferenceManager
import hr.dice.lukac11.weatherapp.enums.Days
import hr.dice.lukac11.weatherapp.enums.Language
import hr.dice.lukac11.weatherapp.enums.Theme
import hr.dice.lukac11.weatherapp.enums.Unit
import hr.dice.lukac11.weatherapp.model.UserPreferences

/**
 * Screen shows [SettingsContent].
 * It passes onClicks from viewModel to [SettingsContent]
 */
@Destination
@Composable
fun Settings(settingsViewModel: SettingsViewModel) {
    val settingState = settingsViewModel.userSetting.collectAsState()

    SettingsContent(
        onThemeClick = { settingsViewModel.saveTheme(it) },
        onLanguageClick = { settingsViewModel.saveLanguage(it) },
        onUnitClick = { settingsViewModel.saveUnit(it) },
        onDaysClick = { settingsViewModel.saveNumberOfDays(it) },
        userSettings = settingState
    )
}

/**
 * Shows list of options presented with segmented buttons
 */
@Composable
fun SettingsContent(
    onThemeClick: (theme: Theme) -> kotlin.Unit,
    onLanguageClick: (language: Language) -> kotlin.Unit,
    onUnitClick: (unit: Unit) -> kotlin.Unit,
    onDaysClick: (days: Days) -> kotlin.Unit,
    userSettings: State<UserPreferences>

) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(top = 60.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            color = MaterialTheme.colorScheme.onBackground,
            text = stringResource(R.string.theme_style),
            modifier = Modifier
                .padding(top = 24.dp, start = 14.dp, bottom = 10.dp)
                .fillMaxWidth(),
            style = MaterialTheme.typography.bodyMedium,
            textAlign = TextAlign.Start,
            fontSize = 14.sp
        )
        val themes = listOf(R.string.light, R.string.dark, R.string.deflt)
        SegmentedButton(
            items = themes,
            defaultSelectedItemIndex = userSettings.value.theme.ordinal,
            onItemSelection = { selectedItemIndex ->
                onThemeClick(Theme.values()[selectedItemIndex])
            },
            cornerRadius = 50,
            useFixedWidth = true,

        )
        Text(
            color = MaterialTheme.colorScheme.onBackground,
            text = stringResource(R.string.lang),
            modifier = Modifier
                .padding(top = 24.dp, start = 14.dp, bottom = 10.dp)
                .fillMaxWidth(),
            style = MaterialTheme.typography.bodySmall,
            textAlign = TextAlign.Start,
            fontSize = 14.sp
        )
        val languages = listOf(R.string.croatian, R.string.english, R.string.german)
        SegmentedButton(
            items = languages,
            defaultSelectedItemIndex = userSettings.value.language.ordinal,
            onItemSelection = { selectedItemIndex ->
                onLanguageClick(Language.values()[selectedItemIndex])
            }, cornerRadius = 50,
            useFixedWidth = true,

        )
        Text(
            color = MaterialTheme.colorScheme.onBackground,
            text = stringResource(R.string.units),
            modifier = Modifier
                .padding(top = 24.dp, start = 14.dp, bottom = 10.dp)
                .fillMaxWidth(),
            style = MaterialTheme.typography.bodySmall,
            textAlign = TextAlign.Start,
            fontSize = 14.sp
        )
        val units = listOf(R.string.standard, R.string.imperial, R.string.metric)
        SegmentedButton(
            items = units,
            defaultSelectedItemIndex = userSettings.value.unit.ordinal,
            onItemSelection = { selectedItemIndex ->
                onUnitClick(Unit.values()[selectedItemIndex])
            },
            cornerRadius = 50,
            useFixedWidth = true,

        )
        Text(
            color = MaterialTheme.colorScheme.onBackground,
            text = stringResource(R.string.number_of_days_display),
            modifier = Modifier
                .padding(top = 24.dp, start = 14.dp, bottom = 10.dp)
                .fillMaxWidth(),
            style = MaterialTheme.typography.bodySmall,
            textAlign = TextAlign.Start,
            fontSize = 14.sp
        )
        val numberOfDays = listOf(R.string.three, R.string.four, R.string.five, R.string.six, R.string.seven)
        SegmentedButton(
            items = numberOfDays,
            defaultSelectedItemIndex = userSettings.value.numberOfDays.ordinal,
            onItemSelection = { selectedItemIndex ->
                onDaysClick(Days.values()[selectedItemIndex])
            },
            cornerRadius = 50,
            itemWidth = 74.dp,
            useFixedWidth = true,

        )
    }
}

@Composable
@Preview
fun PreviewSettingsScreen() {
    Settings(SettingsViewModel(UserSettingsRepository(PreferenceManager(LocalContext.current))))
}
