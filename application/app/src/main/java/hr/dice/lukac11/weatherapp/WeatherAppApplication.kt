package hr.dice.lukac11.weatherapp

import android.app.Application
import hr.dice.lukac11.weatherapp.di.databaseModule
import hr.dice.lukac11.weatherapp.di.networkModule
import hr.dice.lukac11.weatherapp.di.repositoryModule
import hr.dice.lukac11.weatherapp.di.utilsModule
import hr.dice.lukac11.weatherapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin

class WeatherAppApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@WeatherAppApplication)
            modules(
                listOf(
                    viewModelModule, repositoryModule, networkModule, databaseModule,
                    utilsModule
                )
            )
        }
    }
}
