package hr.dice.lukac11.weatherapp.network

import hr.dice.lukac11.weatherapp.model.DailyWeatherModel
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface VisualCrossingWeatherApiService {
    @GET("VisualCrossingWebServices/rest/services/timeline/{location}")
    suspend fun getDailyWeather(
        @Path("location") latLon: String,
        @Query("unitGroup") units: String,
        @Query("lang") language: String,
        @Query("key") apiKey: String
    ): DailyWeatherModel
}
