package hr.dice.lukac11.weatherapp.ui.currentweatherscreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hr.dice.lukac11.weatherapp.data.CurrentWeatherRepository
import hr.dice.lukac11.weatherapp.data.LocationRepository
import hr.dice.lukac11.weatherapp.data.UserSettingsRepository
import hr.dice.lukac11.weatherapp.enums.Unit
import hr.dice.lukac11.weatherapp.model.LocationModel
import hr.dice.lukac11.weatherapp.model.UserPreferences
import hr.dice.lukac11.weatherapp.ui.state.UiState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import okio.IOException

class CurrentWeatherViewModel(private val currentWeatherRepository: CurrentWeatherRepository, private val locationRepository: LocationRepository, private val datastoreRepository: UserSettingsRepository) : ViewModel() {
    private val _uiState = MutableStateFlow<UiState>(UiState.Loading)
    val uiState: StateFlow<UiState> = _uiState
    private val _unitValue = MutableStateFlow<Unit>(Unit.STANDARD)
    val unitValue: StateFlow<Unit> = _unitValue
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private var language: String = ""
    init {
        viewModelScope.launch {
            try {

                locationRepository.getActiveLocation()
                    .combine(datastoreRepository.getPreferenceFlow()) { location: LocationModel, userPreferences: UserPreferences ->
                        latitude = location.latitude
                        longitude = location.longitude
                        language = userPreferences.unit.openWeatherApiValue
                        _unitValue.value = userPreferences.unit
                        val response = currentWeatherRepository.getCityWeatherData(
                            location.latitude, location.longitude,
                            userPreferences.unit.openWeatherApiValue,
                            userPreferences.language.openWeatherApiValue
                        )
                        _uiState.value = UiState.Success(data = response)
                        response.locationName?.let {
                            location.copy(cityName = it)
                        }?.let {
                            locationRepository.updateLocation(it)
                        }
                    }
                    .collect()
            } catch (ioException: IOException) {
                _uiState.value = UiState.InternetError
            } catch (e: Exception) {
                _uiState.value = UiState.Error
            }
        }
    }

    fun loadCityWeatherData() {
        viewModelScope.launch() {
            try {
                val response = currentWeatherRepository.getCityWeatherData(latitude, longitude, _unitValue.value.openWeatherApiValue, language)
                _uiState.value = UiState.Success(data = response)
            } catch (ioException: IOException) {
                _uiState.value = UiState.InternetError
            } catch (e: Exception) {
                _uiState.value = UiState.Error
            }
        }
    }
}
