package hr.dice.lukac11.weatherapp.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.SerialName
@Parcelize
@kotlinx.serialization.Serializable
data class DailyWeatherModel(
    @SerialName("days")
    val locations: List<DailyWeatherInfo> = listOf()
) : Parcelable
