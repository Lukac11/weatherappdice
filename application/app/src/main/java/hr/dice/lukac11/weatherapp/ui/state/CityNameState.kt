package hr.dice.lukac11.weatherapp.ui.state

data class CityNameState(
    val cityName: String = "",
    val isLoading: Boolean = true
)
