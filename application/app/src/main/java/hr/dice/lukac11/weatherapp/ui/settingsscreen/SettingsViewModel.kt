package hr.dice.lukac11.weatherapp.ui.settingsscreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hr.dice.lukac11.weatherapp.data.UserSettingsRepository
import hr.dice.lukac11.weatherapp.enums.Days
import hr.dice.lukac11.weatherapp.enums.Language
import hr.dice.lukac11.weatherapp.enums.Theme
import hr.dice.lukac11.weatherapp.enums.Unit
import hr.dice.lukac11.weatherapp.model.UserPreferences
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class SettingsViewModel(private val datastoreRepository: UserSettingsRepository) : ViewModel() {

    private val _userSettings = MutableStateFlow(
        UserPreferences(Theme.LIGHT, Language.CROATIAN, Unit.STANDARD, Days.THREE)
    )
    val userSetting: StateFlow<UserPreferences> = _userSettings
    init {
        getUserSettings()
    }
    fun saveUnit(unit: Unit) {
        viewModelScope.launch {
            datastoreRepository.saveUnit(unit)
        }
    }
    fun saveLanguage(language: Language) {
        viewModelScope.launch {
            datastoreRepository.saveLanguage(language)
        }
    }
    fun saveTheme(theme: Theme) {
        viewModelScope.launch {
            datastoreRepository.saveTheme(theme)
        }
    }
    fun saveNumberOfDays(numberOfDays: Days) {
        viewModelScope.launch {
            datastoreRepository.saveNumberOfDays(numberOfDays)
        }
    }
    private fun getUserSettings() {
        viewModelScope.launch {
            datastoreRepository.getPreferenceFlow().collect { userPreferences ->
                _userSettings.value = userPreferences
            }
        }
    }
}
