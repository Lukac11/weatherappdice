package hr.dice.lukac11.weatherapp.navigation

import androidx.navigation.NavType

data class NavigationArgument<T>(
    val name: String,
    val type: NavType<T>
)
