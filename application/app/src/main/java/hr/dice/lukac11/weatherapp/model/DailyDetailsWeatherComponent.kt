package hr.dice.lukac11.weatherapp.model

import androidx.annotation.StringRes

sealed class DailyDetailsWeatherComponent
data class DailyDetailsCard(val temperature: String, val description: String, val icon: String, val iconUrl: String = "https://raw.githubusercontent.com/visualcrossing/WeatherIcons/main/PNG/2nd%20Set%20-%20Color/$icon.png") : DailyDetailsWeatherComponent()
data class DailyDetailsTitle(@StringRes val title: Int) : DailyDetailsWeatherComponent()
data class DailyDetailsItem(@StringRes val description: Int, val value: String) : DailyDetailsWeatherComponent()
