package hr.dice.lukac11.weatherapp.ui.topbar

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SmallTopAppBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import hr.dice.lukac11.weatherapp.R
import hr.dice.lukac11.weatherapp.ui.destinations.DailyDetailsWeatherScreenDestination
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

/**
 * Screen shows a TopAppBar and state handling for data that is presented on this screen.
 * It uses data given from TopBarViewModel and puts it onto UI.
 * @param topBarViewModel is used for getting data
 * @param currentRoute is used to get to know what is current destination route
 * @param navController is used to navigate up to previous screen
 */
@Composable
fun TopBar(topBarViewModel: TopBarViewModel, currentRoute: String?, navController: NavController) {
    topBarViewModel.getCityName()
    val cityState by topBarViewModel.cityState.collectAsState()
    val date by topBarViewModel.date.collectAsState()
    if (!cityState.isLoading) {
        if (currentRoute == DailyDetailsWeatherScreenDestination.route) {
            DailyDetailsTopBar(
                cityName = cityState.cityName,
                date = date,
                onBackArrowClick = { navController.navigateUp() }
            )
        } else {
            MainScreenTopBar(cityName = cityState.cityName)
        }
    }
}

/**
 * Used as TopAppBar for screens within Main Bottom Navigation Screen Container
 * @param cityName is used for presenting name of the city on Text
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreenTopBar(cityName: String) {
    SmallTopAppBar(
        title = {
            Text(text = cityName)
        },
        modifier = Modifier.fillMaxWidth(),
        actions = {
            IconButton(onClick = { /* doSomething() */ }) {
                Icon(
                    painter = painterResource(
                        id =
                        R.drawable.ic_outline_edit_location_alt_24
                    ),
                    contentDescription = null,
                    modifier = Modifier.size(32.dp),
                    tint = MaterialTheme.colorScheme.background
                )
            }
        }
    )
}

@Preview
@Composable
private fun PreviewMainScreenTopBar() {
    MainScreenTopBar(cityName = "Đakovo")
}

/**
 * Used as TopAppBar for DailyDetailsWeatherScreen
 * @param cityName is used for presenting name of the city on Text
 * @param date is used for presenting date in the year on Text
 * @param onBackArrowClick callback used to navigate up to previous screen
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DailyDetailsTopBar(cityName: String, date: LocalDate, onBackArrowClick: () -> Unit) {
    SmallTopAppBar(
        title = {
            Text(text = "$cityName - ${date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT))}")
        },
        navigationIcon = {
            IconButton(onClick = onBackArrowClick) {
                Icon(imageVector = Icons.Filled.ArrowBack, contentDescription = null)
            }
        }
    )
}

@Preview
@Composable
private fun PreviewDailyDetailsTopBar() {
    DailyDetailsTopBar(cityName = "Đakovo", date = LocalDate.parse("18:15"), onBackArrowClick = {})
}
