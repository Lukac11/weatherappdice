package hr.dice.lukac11.weatherapp.ui.theme

import androidx.compose.ui.graphics.Color

val black = Color(0xFF000000)
val white = Color(0xFFFFFFFF)
val tiber = Color(0xFF06283D)
val tarawera = Color(0xFF072D45)
val denim = Color(0xFF1363DF)
val dodgerBlue = Color(0xFF47B5FF)
val pattensBlue = Color(0xFFDFF6FF)
val tutu = Color(0xFDFFFFBFE)
