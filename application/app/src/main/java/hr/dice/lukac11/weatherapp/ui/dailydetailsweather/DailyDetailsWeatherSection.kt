package hr.dice.lukac11.weatherapp.ui.dailydetailsweather

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import hr.dice.lukac11.weatherapp.R
import hr.dice.lukac11.weatherapp.model.DailyDetailsTitle
/**
 * Shows type of weather information that will be provided
 */
@Composable
fun DailyDetailsWeatherSection(dailyDetailsTitle: DailyDetailsTitle) {
    Text(text = stringResource(id = dailyDetailsTitle.title), color = MaterialTheme.colorScheme.onBackground, fontSize = 24.sp, modifier = Modifier.background(color = MaterialTheme.colorScheme.background).fillMaxWidth().padding(vertical = 8.dp), textAlign = TextAlign.Start)
}

@Composable
@Preview
private fun PreviewDailyDetailsWeatherSection() {
    DailyDetailsWeatherSection(DailyDetailsTitle(R.string.temperature))
}
