package hr.dice.lukac11.weatherapp.data

import hr.dice.lukac11.weatherapp.database.PreferenceManager
import hr.dice.lukac11.weatherapp.enums.Days
import hr.dice.lukac11.weatherapp.enums.Language
import hr.dice.lukac11.weatherapp.enums.Theme
import hr.dice.lukac11.weatherapp.enums.Unit
import hr.dice.lukac11.weatherapp.model.UserPreferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

class UserSettingsRepository(private val preferenceManager: PreferenceManager) {
    suspend fun getPreferenceFlow(): Flow<UserPreferences> = withContext(Dispatchers.IO) {
        preferenceManager.preferencesFlow
    }

    suspend fun saveUnit(unit: Unit) {
        withContext(Dispatchers.IO) {
            preferenceManager.updateUnits(unit)
        }
    }
    suspend fun saveLanguage(language: Language) {
        withContext(Dispatchers.IO) {
            preferenceManager.updateLanguage(language)
        }
    }
    suspend fun saveTheme(theme: Theme) {
        withContext(Dispatchers.IO) {
            preferenceManager.updateTheme(theme)
        }
    }
    suspend fun saveNumberOfDays(numberOfDays: Days) {
        withContext(Dispatchers.IO) {
            preferenceManager.updateNumberOfDays(numberOfDays)
        }
    }
}
