package hr.dice.lukac11.weatherapp.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hr.dice.lukac11.weatherapp.data.LocationRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class StartLoaderViewModel(private val locationRepository: LocationRepository) : ViewModel() {
    private val _locationState = MutableStateFlow<Boolean?>(null)
    val locationState: StateFlow<Boolean?> = _locationState
    init {
        checkIfLocationIsAvailable()
    }

    private fun checkIfLocationIsAvailable() {
        viewModelScope.launch {
            _locationState.value = locationRepository.checkIfLocationAvailable()
        }
    }
}
