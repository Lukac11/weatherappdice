package hr.dice.lukac11.weatherapp.database

import androidx.room.Database
import androidx.room.RoomDatabase
import hr.dice.lukac11.weatherapp.model.LocationModel

@Database(entities = [LocationModel::class], version = 3, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun locationDao(): LocationDao
}
