package hr.dice.lukac11.weatherapp.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import hr.dice.lukac11.weatherapp.enums.Theme
import hr.dice.lukac11.weatherapp.ui.settingsscreen.SettingsViewModel
import org.koin.androidx.compose.getViewModel

private val DarkColorPalette = darkColorScheme(
    surface = denim,
    onSurface = white,
    background = tarawera,
    onBackground = pattensBlue
)

private val LightColorPalette = lightColorScheme(
    background = tutu,
    onBackground = tarawera,
    surface = denim,
    onSurface = white
)

@Composable
fun WeatherAppTheme(theme: Theme = currentTheme(), content: @Composable () -> Unit) {
    val colors = when (theme) {
        Theme.LIGHT -> LightColorPalette
        Theme.DARK -> DarkColorPalette
        Theme.DEFAULT -> {
            if (isSystemInDarkTheme()) DarkColorPalette
            else LightColorPalette
        }
    }

    MaterialTheme(
        colorScheme = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}

@Composable
fun currentTheme(): Theme {
    val viewModel = getViewModel<SettingsViewModel>()
    val userSettings by viewModel.userSetting.collectAsState()
    return userSettings.theme
}
