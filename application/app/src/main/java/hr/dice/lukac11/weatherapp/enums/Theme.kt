package hr.dice.lukac11.weatherapp.enums

enum class Theme {
    LIGHT,
    DARK,
    DEFAULT
}
