package hr.dice.lukac11.weatherapp.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Wind(
    @SerialName("speed")
    val speed: Double? = null,
    @SerialName("deg")
    val degree: Int? = null
)
