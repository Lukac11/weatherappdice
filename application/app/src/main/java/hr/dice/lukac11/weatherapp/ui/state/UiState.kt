package hr.dice.lukac11.weatherapp.ui.state

sealed interface UiState {
    object Loading : UiState
    data class Success<T>(val data: T) : UiState
    object Error : UiState
    object InternetError : UiState
}
