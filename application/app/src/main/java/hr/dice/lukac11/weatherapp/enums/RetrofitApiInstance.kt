package hr.dice.lukac11.weatherapp.enums

enum class RetrofitApiInstance(val baseUrl: String, val apiKey: String) {
    OPEN_WEATHER_API(baseUrl = "http://api.openweathermap.org/", apiKey = "d649a0e2c764499b3da8155b1869045d"),
    VISUAL_CROSSING_API(baseUrl = "https://weather.visualcrossing.com/", apiKey = "VMT7QXCSLYKMVAVCNM6NF73SB")
}
