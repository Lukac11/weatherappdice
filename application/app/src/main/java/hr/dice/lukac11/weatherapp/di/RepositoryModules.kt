package hr.dice.lukac11.weatherapp.di

import hr.dice.lukac11.weatherapp.data.CurrentWeatherRepository
import hr.dice.lukac11.weatherapp.data.DailyWeatherRepository
import hr.dice.lukac11.weatherapp.data.LocationRepository
import hr.dice.lukac11.weatherapp.data.UserSettingsRepository
import org.koin.dsl.module

val repositoryModule = module {
    single { CurrentWeatherRepository(get()) }
    single { LocationRepository(get()) }
    single { DailyWeatherRepository(get()) }
    single { UserSettingsRepository(get()) }
}
