package hr.dice.lukac11.weatherapp.enums

enum class Days(val value: Int) {
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7)
}
