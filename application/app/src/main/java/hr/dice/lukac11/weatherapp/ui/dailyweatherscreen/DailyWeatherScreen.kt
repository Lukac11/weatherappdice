package hr.dice.lukac11.weatherapp.ui.dailyweatherscreen
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.HorizontalPagerIndicator
import com.google.accompanist.pager.rememberPagerState
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.navigate
import hr.dice.lukac11.weatherapp.model.DailyWeatherInfo
import hr.dice.lukac11.weatherapp.model.DailyWeatherModel
import hr.dice.lukac11.weatherapp.ui.destinations.DailyDetailsWeatherScreenDestination
import hr.dice.lukac11.weatherapp.ui.errorscreen.ErrorContent
import hr.dice.lukac11.weatherapp.ui.loadingscreen.LoadingScreen
import hr.dice.lukac11.weatherapp.ui.nointernetconnectionscreen.NoInternetConnectionContent
import hr.dice.lukac11.weatherapp.ui.state.UiState
import hr.dice.lukac11.weatherapp.ui.topbar.TopBarViewModel
import org.koin.androidx.compose.getViewModel

/**
 * Screen shows a [DailyWeatherContent] and state handling for data that is presented on this screen.
 * It uses data given from dailyWeatherViewModel and puts it onto UI.
 */
@OptIn(ExperimentalAnimationApi::class)
@Destination
@Composable
fun DailyWeatherScreen(navController: NavController, topBarViewModel: TopBarViewModel) {
    val viewModel = getViewModel<DailyWeatherViewModel>()
    val uiState by viewModel.uiState.collectAsState()
    val unitPosition by viewModel.unitValue.collectAsState()
    val numberOfDays by viewModel.numberOfDays.collectAsState()

    AnimatedContent(targetState = uiState) { targetState ->
        when (targetState) {
            UiState.Loading -> LoadingScreen()
            UiState.InternetError -> NoInternetConnectionContent {
                viewModel.loadCityWeatherData()
            }
            UiState.Error -> ErrorContent {
                viewModel.loadCityWeatherData()
            }
            is UiState.Success<*> -> DailyWeatherContent(
                targetState.data as DailyWeatherModel,
                navigateToDetails = {
                    topBarViewModel.setDate(targetState.data.locations[it].localDate)
                    navController.navigate(DailyDetailsWeatherScreenDestination(targetState.data.locations[it]))
                },
                unitPosition,
                numberOfDays.value
            )
        }
    }
}
/**
 * Shows information about daily weather data on Screen via pager layouts.
 * @param dailyWeatherModel is used for presenting all information from api to screen and it is passed onto [CardContent].
 */
@OptIn(ExperimentalPagerApi::class)
@Composable
fun DailyWeatherContent(dailyWeatherModel: DailyWeatherModel, navigateToDetails: (page: Int) -> Unit, unitPositionValue: hr.dice.lukac11.weatherapp.enums.Unit, numberOfDays: Int) {
    val pagerState = rememberPagerState()
    Column(
        modifier = Modifier
            .background(color = MaterialTheme.colorScheme.background)
            .fillMaxSize(),
        verticalArrangement = Arrangement.Center
    ) {
        HorizontalPager(count = numberOfDays, state = pagerState) { page ->
            CardContent(dailyWeatherModel = dailyWeatherModel, page = page, navigateToDetails = navigateToDetails, unitPosition = unitPositionValue)
        }

        HorizontalPagerIndicator(
            pagerState = pagerState,
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(32.dp),
            activeColor = MaterialTheme.colorScheme.surface,
            inactiveColor = MaterialTheme.colorScheme.onBackground,
            indicatorWidth = 16.dp
        )
    }
}

@Preview
@Composable
private fun PreviewDailyWeatherContent() {
    DailyWeatherContent(
        dailyWeatherModel = DailyWeatherModel(
            locations = listOf(
                DailyWeatherInfo(
                    "2022-12-15", 4.3, 31.7, 22.9,
                    1671062400, 27.3, 1011.2, 81.2, "rain", "overcast", 11.2, 27.12, "8.11", "18.11", 112.01
                )
            )
        ),
        navigateToDetails = {}, unitPositionValue = hr.dice.lukac11.weatherapp.enums.Unit.STANDARD, numberOfDays = 5
    )
}
