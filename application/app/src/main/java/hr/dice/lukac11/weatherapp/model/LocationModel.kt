package hr.dice.lukac11.weatherapp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
const val TN_LOCATION_MODEl = "location"
const val COL_LOCATION_MODEL_CITY_NAME = "city_name"
const val COL_LOCATION_MODEL_LATITUDE = "latitude"
const val COL_LOCATION_MODEL_LONGITUDE = "longitude"
const val COL_LOCATION_MODEL_IS_ACTIVE = "is_active"
const val COL_LOCATION_MODEL_ID = "id"

@Entity(tableName = TN_LOCATION_MODEl)
data class LocationModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = COL_LOCATION_MODEL_ID)
    val id: Int = 0,
    @ColumnInfo(name = COL_LOCATION_MODEL_LATITUDE)
    val latitude: Double,
    @ColumnInfo(name = COL_LOCATION_MODEL_LONGITUDE)
    val longitude: Double,
    @ColumnInfo(name = COL_LOCATION_MODEL_CITY_NAME)
    val cityName: String,
    @ColumnInfo(name = COL_LOCATION_MODEL_IS_ACTIVE)
    val isActive: Boolean = false
)
