package hr.dice.lukac11.weatherapp.ui.mainscreen

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.manualcomposablecalls.composable
import hr.dice.lukac11.weatherapp.navigation.BottomNavigationBar
import hr.dice.lukac11.weatherapp.ui.NavGraphs
import hr.dice.lukac11.weatherapp.ui.currentweatherscreen.CurrentWeatherScreen
import hr.dice.lukac11.weatherapp.ui.dailyweatherscreen.DailyWeatherScreen
import hr.dice.lukac11.weatherapp.ui.destinations.CurrentWeatherScreenDestination
import hr.dice.lukac11.weatherapp.ui.destinations.DailyWeatherScreenDestination
import hr.dice.lukac11.weatherapp.ui.destinations.SettingsDestination
import hr.dice.lukac11.weatherapp.ui.settingsscreen.Settings
import hr.dice.lukac11.weatherapp.ui.settingsscreen.SettingsViewModel
import hr.dice.lukac11.weatherapp.ui.topbar.TopBar
import hr.dice.lukac11.weatherapp.ui.topbar.TopBarViewModel
import org.koin.androidx.compose.getViewModel

/**
 * Screen that holds bottom navigation with its screens. It contains its own navController and BottomNavigationBar is assigned to bottomBar.
 */
@Destination
@OptIn(ExperimentalMaterial3Api::class, ExperimentalAnimationApi::class)
@Composable
fun MainBottomNavBarScreen() {
    val navController: NavHostController = rememberNavController()
    val backStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute = backStackEntry?.destination?.route
    val topBarViewModel = getViewModel<TopBarViewModel>()
    val settingsViewModel = getViewModel<SettingsViewModel>()

    Scaffold(
        topBar = { TopBar(topBarViewModel, currentRoute, navController) },
        bottomBar = { BottomNavigationBar(navController = navController) },
        modifier = Modifier.background(MaterialTheme.colorScheme.background)
    ) {
        BottomNavBarGraph(navController, it, topBarViewModel, settingsViewModel)
    }
}

/**
 * Navigation graph for bottom navigation bar which contains screens [CurrentWeatherScreen], [DailyWeatherScreen] and [Settings].
 * @param navController of type [NavHostController], it is used for navigation of bottom nav bar
 */

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun BottomNavBarGraph(navController: NavHostController, paddingValues: PaddingValues, topBarViewModel: TopBarViewModel, settingsViewModel: SettingsViewModel) {

    DestinationsNavHost(navGraph = NavGraphs.root, startRoute = CurrentWeatherScreenDestination, navController = navController) {
        composable(CurrentWeatherScreenDestination) {
            CurrentWeatherScreen()
        }
        composable(DailyWeatherScreenDestination) {
            DailyWeatherScreen(navController, topBarViewModel)
        }
        composable(SettingsDestination) {
            Settings(settingsViewModel)
        }
    }
}
