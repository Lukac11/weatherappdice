package hr.dice.lukac11.weatherapp.ui.dailydetailsweather

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hr.dice.lukac11.weatherapp.R
import hr.dice.lukac11.weatherapp.data.UserSettingsRepository
import hr.dice.lukac11.weatherapp.enums.Unit
import hr.dice.lukac11.weatherapp.model.DailyDetailsCard
import hr.dice.lukac11.weatherapp.model.DailyDetailsItem
import hr.dice.lukac11.weatherapp.model.DailyDetailsTitle
import hr.dice.lukac11.weatherapp.model.DailyDetailsWeatherComponent
import hr.dice.lukac11.weatherapp.model.DailyWeatherInfo
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class DailyDetailsWeatherViewModel(private val userSettingsRepository: UserSettingsRepository, private val dailyWeatherInfo: DailyWeatherInfo) : ViewModel() {
    private val _dailyDetailsWeatherComponentList = MutableStateFlow(emptyList<DailyDetailsWeatherComponent>())
    val dailyDetailsWeatherComponentList: StateFlow<List<DailyDetailsWeatherComponent>> = _dailyDetailsWeatherComponentList
    private var unitValue: Unit = Unit.STANDARD

    init {
        setDailyWeatherInfo()
    }

    fun setDailyWeatherInfo() {
        viewModelScope.launch {
            userSettingsRepository.getPreferenceFlow().collect { userPreferences ->
                unitValue = userPreferences.unit
                _dailyDetailsWeatherComponentList.value = listOf(
                    DailyDetailsCard(temperature = "${dailyWeatherInfo.temp.toInt()}${unitValue.temperature}", description = dailyWeatherInfo.conditions, icon = dailyWeatherInfo.icon),
                    DailyDetailsTitle(title = R.string.temperature),
                    DailyDetailsItem(description = R.string.minimal_temperature_lbl, value = dailyWeatherInfo.tempMin.toInt().toString() + unitValue.temperature),
                    DailyDetailsItem(description = R.string.max_temperature_lbl, value = dailyWeatherInfo.tempMax.toInt().toString() + unitValue.temperature),
                    DailyDetailsTitle(title = R.string.feels_like_temperature),
                    DailyDetailsItem(description = R.string.feels_like_min, value = dailyWeatherInfo.feelsLikeMin.toInt().toString() + unitValue.temperature),
                    DailyDetailsItem(description = R.string.feels_like_max, value = dailyWeatherInfo.feelsLikeMax.toInt().toString() + unitValue.temperature),
                    DailyDetailsTitle(title = R.string.sun),
                    DailyDetailsItem(description = R.string.sunrise, value = dailyWeatherInfo.sunrise),
                    DailyDetailsItem(description = R.string.sunset, value = dailyWeatherInfo.sunset),
                    DailyDetailsTitle(title = R.string.wind),
                    DailyDetailsItem(description = R.string.speed, value = dailyWeatherInfo.windSpeed.toInt().toString() + unitValue.speed),
                    DailyDetailsItem(description = R.string.windDirection, value = dailyWeatherInfo.newWindDirection)
                )
            }
        }
    }
}
