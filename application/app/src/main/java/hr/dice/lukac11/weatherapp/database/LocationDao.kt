package hr.dice.lukac11.weatherapp.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import hr.dice.lukac11.weatherapp.model.COL_LOCATION_MODEL_IS_ACTIVE
import hr.dice.lukac11.weatherapp.model.LocationModel
import hr.dice.lukac11.weatherapp.model.TN_LOCATION_MODEl
import kotlinx.coroutines.flow.Flow

@Dao
interface LocationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertLocation(locationModel: LocationModel)

    @Query("SELECT * FROM $TN_LOCATION_MODEl where $COL_LOCATION_MODEL_IS_ACTIVE == 1 LIMIT 1")
    fun getActiveLocation(): Flow<LocationModel>

    @Update
    suspend fun updateLocation(locationModel: LocationModel)

    @Query("SELECT EXISTS(SELECT * FROM $TN_LOCATION_MODEl)")
    suspend fun checkIfLocationAvailable(): Boolean
}
