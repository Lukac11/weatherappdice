package hr.dice.lukac11.weatherapp.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.navigation.popUpTo
import hr.dice.lukac11.weatherapp.ui.destinations.MainBottomNavBarScreenDestination
import hr.dice.lukac11.weatherapp.ui.destinations.NoLocationSavedScreenDestination
import hr.dice.lukac11.weatherapp.ui.destinations.StartLoaderScreenDestination
import hr.dice.lukac11.weatherapp.ui.loadingscreen.LoadingScreen
import org.koin.androidx.compose.getViewModel

/**
 * Application starting screen which is used as start destination for Navigation Compose.
 */
@RootNavGraph(start = true)
@Destination
@Composable
fun StartLoaderScreen(navController: DestinationsNavigator) {
    val viewModel = getViewModel<StartLoaderViewModel>()
    val locationState by viewModel.locationState.collectAsState()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        when (locationState) {
            null -> LoadingScreen()
            false -> navController.navigate(NoLocationSavedScreenDestination) {
                popUpTo(StartLoaderScreenDestination) {
                    inclusive = true
                }
            }
            true -> navController.navigate(MainBottomNavBarScreenDestination) {
                popUpTo(StartLoaderScreenDestination) {
                    inclusive = true
                }
            }
        }
    }
}
