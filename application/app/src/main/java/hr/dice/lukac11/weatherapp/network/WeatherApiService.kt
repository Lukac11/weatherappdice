package hr.dice.lukac11.weatherapp.network

import hr.dice.lukac11.weatherapp.model.WeatherModel
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApiService {
    @GET("data/2.5/weather")
    suspend fun getCityWeather(@Query("lat") lat: Double, @Query("lon") lon: Double, @Query("appid") apiKey: String, @Query("units") units: String, @Query("lang") language: String): WeatherModel
}
