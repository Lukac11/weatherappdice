package hr.dice.lukac11.weatherapp.ui.nointernetconnectionscreen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import hr.dice.lukac11.weatherapp.R
import hr.dice.lukac11.weatherapp.ui.theme.denim
import hr.dice.lukac11.weatherapp.ui.theme.tutu
/**
 * Composoble that represents NoInternetConnectionScreen
 * @param onTryAgainClick is the callback that will run to do action when "Try Again" is clicked
 */
@Composable
fun NoInternetConnectionContent(onTryAgainClick: () -> Unit) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(10.dp),
        horizontalAlignment = Alignment.CenterHorizontally,

    ) {
        Spacer(modifier = Modifier.weight(1F))
        Icon(
            painter = painterResource(id = R.drawable.ic_outline_wifi_off_24),
            contentDescription = null,
            modifier = Modifier
                .size(128.dp)
                .fillMaxWidth()
        )
        Text(
            color = MaterialTheme.colorScheme.onBackground,
            text = stringResource(R.string.somethingUnexpectedHappened),
            modifier = Modifier
                .padding(top = 10.dp)
                .fillMaxWidth(),
            style = MaterialTheme.typography.headlineLarge,
            textAlign = TextAlign.Center
        )
        Text(
            color = MaterialTheme.colorScheme.onBackground,
            text = stringResource(R.string.checkInternetConnection),
            modifier = Modifier
                .padding(top = 6.dp)
                .fillMaxWidth(),
            style = MaterialTheme.typography.titleLarge,
            textAlign = TextAlign.Center
        )
        Spacer(modifier = Modifier.weight(1F))
        Button(
            onClick = onTryAgainClick,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 74.dp),
            shape = CutCornerShape(10),
            colors = ButtonDefaults.buttonColors(containerColor = denim)
        ) {
            Text(text = stringResource(R.string.tryAgain), color = tutu)
        }
    }
}

@Preview
@Composable
private fun PreviewNoInternetConnectionScreen() {
    NoInternetConnectionContent({})
}
