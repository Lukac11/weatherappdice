package hr.dice.lukac11.weatherapp.ui.dailydetailsweather

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import hr.dice.lukac11.weatherapp.R
import hr.dice.lukac11.weatherapp.model.DailyDetailsItem

/**
 * Shows description and value of weather information
 */
@Composable
fun DailyDetailsWeatherItem(dailyDetailsItem: DailyDetailsItem) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = MaterialTheme.colorScheme.background)
            .padding(4.dp)
    ) {
        Text(text = stringResource(id = dailyDetailsItem.description), fontSize = 14.sp, fontWeight = FontWeight.Light, color = MaterialTheme.colorScheme.onBackground)
        Text(text = dailyDetailsItem.value, fontSize = 14.sp, fontWeight = FontWeight.Bold, color = MaterialTheme.colorScheme.onBackground)
        Divider(
            color = MaterialTheme.colorScheme.onBackground,
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp)
        )
    }
}

@Composable
@Preview
private fun privateDailyDetailsWeatherItem() {
    DailyDetailsWeatherItem(DailyDetailsItem(R.string.temperature, "50%"))
}
