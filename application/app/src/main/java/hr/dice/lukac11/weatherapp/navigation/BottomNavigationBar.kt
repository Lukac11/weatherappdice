package hr.dice.lukac11.weatherapp.navigation

import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationBarItemDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.ramcosta.composedestinations.navigation.navigate
import hr.dice.lukac11.weatherapp.ui.destinations.DailyDetailsWeatherScreenDestination

/**
 * Composable that makes BottomNavBar with all its icons and labels.
 * It handles navigation of 3 screens that are in bottom navigation.
 * Keeps single instance of screen on backstack.
 */
@Composable
fun BottomNavigationBar(navController: NavHostController) {
    val backStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute = backStackEntry?.destination?.route
    if (currentRoute != DailyDetailsWeatherScreenDestination.route) {
        BottomAppBar(
            containerColor = MaterialTheme.colorScheme.background
        ) {
            enumValues<BarItem>().forEach { navItem ->

                NavigationBarItem(
                    selected = currentRoute == navItem.navRoute.route,
                    onClick = {
                        navController.navigate(navItem.navRoute) {
                            popUpTo(navController.graph.findStartDestination().id) {
                                saveState = true
                            }
                            launchSingleTop = true
                            restoreState = true
                        }
                    },

                    icon = {
                        Icon(
                            painter = painterResource(id = navItem.icon),
                            contentDescription = null
                        )
                    },
                    label = { Text(text = stringResource(id = navItem.label)) },
                    alwaysShowLabel = true,
                    colors = NavigationBarItemDefaults.colors(selectedTextColor = MaterialTheme.colorScheme.onBackground, selectedIconColor = MaterialTheme.colorScheme.onBackground)

                )
            }
        }
    }
}
