package hr.dice.lukac11.weatherapp.ui.nolocationsavedscreen

import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionStatus
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import com.google.accompanist.permissions.shouldShowRationale
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.Priority
import com.google.android.gms.tasks.CancellationTokenSource
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.navigation.popUpTo
import hr.dice.lukac11.weatherapp.R
import hr.dice.lukac11.weatherapp.ui.destinations.MainBottomNavBarScreenDestination
import hr.dice.lukac11.weatherapp.ui.destinations.NoLocationSavedScreenDestination
import hr.dice.lukac11.weatherapp.ui.loadingscreen.LoadingScreen
import hr.dice.lukac11.weatherapp.ui.nolocationscreen.NoLocationScreen
import hr.dice.lukac11.weatherapp.ui.state.UiState
import hr.dice.lukac11.weatherapp.ui.theme.WeatherAppTheme
import hr.dice.lukac11.weatherapp.ui.theme.denim
import hr.dice.lukac11.weatherapp.ui.theme.tutu
import org.koin.androidx.compose.getViewModel

/**
 * Screen shows a [NoLocationSavedContent] and handles runtime permission for location.
 */
@Destination
@SuppressLint("MissingPermission")
@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun NoLocationSavedScreen(navController: DestinationsNavigator) {
    val noLocationSavedViewModel = getViewModel<NoLocationSavedViewModel>()
    val uiState by noLocationSavedViewModel.uiState.collectAsState()
    val locationPermissionState = rememberPermissionState(
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    var shouldNavigate by rememberSaveable {
        mutableStateOf(false)
    }
    when (uiState) {
        UiState.Loading -> LoadingScreen()
        UiState.Error -> NoLocationScreen(navController = navController)
        is UiState.Success<*> -> LaunchedEffect(Unit) {
            navController.navigate(MainBottomNavBarScreenDestination) {
                popUpTo(NoLocationSavedScreenDestination) {
                    inclusive = true
                }
            }
        }
        else -> {
            NoLocationSavedContent(onShareCurrentLocationClick = {
                when (locationPermissionState.status) {
                    PermissionStatus.Granted -> {
                        noLocationSavedViewModel.getLocation()
                    }
                    is PermissionStatus.Denied -> {
                        // TODO: Navigate to No location screen
                        locationPermissionState.launchPermissionRequest()
                        shouldNavigate = true
                    }
                }
            }, onSearchLocationsClick = {
                // TODO: Navigate to another screen for Location selection
            })
        }
    }
    if (locationPermissionState.status.isGranted && shouldNavigate) {
        noLocationSavedViewModel.getLocation()
        shouldNavigate = false
    }
    if (!locationPermissionState.status.isGranted && locationPermissionState.status.shouldShowRationale) {
        noLocationSavedViewModel.updateUIStateToError()
    }
}

/**
 * Shows a information when there is no available location for showing weather data.
 *
 * It has interaction options like sharing current location or searching locations.
 *
 * @param onShareCurrentLocationClick callback invoked when user requested sharing current location.
 * @param onSearchLocationsClick callback invoked when user requested searching locations.
 */
@Composable
private fun NoLocationSavedContent(
    onShareCurrentLocationClick: () -> Unit,
    onSearchLocationsClick: () -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(horizontal = 10.dp),
        horizontalAlignment = Alignment.CenterHorizontally,

    ) {
        Spacer(modifier = Modifier.weight(1F))
        Icon(
            painter = painterResource(id = R.drawable.ic_round_my_location_24_white),
            tint = MaterialTheme.colorScheme.onBackground,
            contentDescription = null,
            modifier = Modifier
                .size(128.dp)
                .fillMaxWidth(),
        )
        Text(
            color = MaterialTheme.colorScheme.onBackground,
            text = stringResource(R.string.noLocationStored),
            modifier = Modifier
                .padding(top = 10.dp)
                .fillMaxWidth(),
            style = MaterialTheme.typography.headlineLarge,
            textAlign = TextAlign.Center
        )
        Text(
            color = MaterialTheme.colorScheme.onBackground,
            text = stringResource(R.string.locationNeeded),
            modifier = Modifier
                .padding(top = 5.dp)
                .fillMaxWidth(),
            style = MaterialTheme.typography.titleLarge,
            textAlign = TextAlign.Center
        )
        Spacer(modifier = Modifier.weight(1F))
        Button(
            onClick = onShareCurrentLocationClick,
            modifier = Modifier
                .fillMaxWidth(),
            shape = CutCornerShape(10),
            colors = ButtonDefaults.buttonColors(containerColor = denim)
        ) {
            Text(text = stringResource(R.string.shareLocation), color = tutu)
        }
        Button(
            onClick = onSearchLocationsClick,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 10.dp),
            shape = CutCornerShape(10),
            colors = ButtonDefaults.buttonColors(containerColor = denim)
        ) {
            Text(text = stringResource(R.string.searchLocation), color = tutu)
        }
    }
}

@Preview
@Composable
private fun PreviewNoLocationSavedScreen() {
    WeatherAppTheme {
        NoLocationSavedContent(
            onShareCurrentLocationClick = { /*TODO*/ },
            onSearchLocationsClick = {}
        )
    }
}

@SuppressLint("MissingPermission")
fun FusedLocationProviderClient.getLocation(onLocationSuccess: (Location) -> Unit, onLocationFailed: () -> Unit) {
    val cancellationTokenSource = CancellationTokenSource()
    this.getCurrentLocation(
        Priority.PRIORITY_HIGH_ACCURACY,
        cancellationTokenSource.token
    ).addOnSuccessListener {
        if (it != null) {
            onLocationSuccess(it)
        } else {
            onLocationFailed()
        }
        cancellationTokenSource.cancel()
    }
}
