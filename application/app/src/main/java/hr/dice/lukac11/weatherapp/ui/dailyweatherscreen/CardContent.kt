package hr.dice.lukac11.weatherapp.ui.dailyweatherscreen

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import hr.dice.lukac11.weatherapp.R
import hr.dice.lukac11.weatherapp.model.DailyWeatherInfo
import hr.dice.lukac11.weatherapp.model.DailyWeatherModel
import hr.dice.lukac11.weatherapp.ui.currentweatherscreen.RowElement

/**
 * Shows information about daily weather on a card.
 * @param dailyWeatherModel is used to present information from api to screen.
 * @param page is used for knowing which index of list is accessed.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CardContent(dailyWeatherModel: DailyWeatherModel, page: Int, navigateToDetails: (page: Int) -> Unit, unitPosition: hr.dice.lukac11.weatherapp.enums.Unit) {
    Card(
        border = BorderStroke(1.dp, MaterialTheme.colorScheme.onBackground),
        modifier = Modifier
            .fillMaxWidth()
            // .align(Alignment.CenterHorizontally)
            .padding(horizontal = 24.dp, vertical = 50.dp),
        shape = RoundedCornerShape(24.dp),
        onClick = {
            navigateToDetails(page)
        }
    ) {
        Column(
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.background)
                .padding(18.dp)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,

        ) {
            Text(
                text = when (page) {
                    0 -> stringResource(id = R.string.today)
                    1 -> stringResource(id = R.string.tomorrow)
                    else -> {
                        dailyWeatherModel.locations[page].dayOfTheWeek
                    }
                },
                style = MaterialTheme.typography.displayMedium,
                color = MaterialTheme.colorScheme.onBackground,
                fontWeight = FontWeight.Bold
            )
            Text(
                text = dailyWeatherModel.locations[page].dateFormatted,
                fontSize = 24.sp,
                modifier = Modifier.padding(bottom = 36.dp),
                color = MaterialTheme.colorScheme.onBackground
            )
            Text(
                text = "${dailyWeatherModel.locations.get(page).temp.toInt()}${unitPosition.temperature}",
                fontSize = 56.sp,
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colorScheme.onBackground,
                modifier = Modifier.padding(bottom = 16.dp)
            )
            Row(
                modifier = Modifier
                    .padding(vertical = 22.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                Text(
                    text = "${dailyWeatherModel.locations[page].tempMin.toInt()}${unitPosition.temperature}",
                    fontSize = 28.sp
                )
                Text(
                    text = "${dailyWeatherModel.locations[page].tempMax.toInt()}${unitPosition.temperature}",
                    fontSize = 28.sp
                )
            }
            Row(
                modifier = Modifier
                    .padding(top = 24.dp, bottom = 12.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                RowElement(
                    weatherParameter = "${dailyWeatherModel.locations[page].humidity.toInt()}%",
                    description = stringResource(id = R.string.humidity)
                )
                RowElement(
                    weatherParameter = "${dailyWeatherModel.locations[page].pressure.toInt()}${unitPosition.pressure}",
                    description = stringResource(id = R.string.pressure)
                )
                RowElement(
                    weatherParameter = "${dailyWeatherModel.locations[page].windSpeed.toInt()}${unitPosition.speed}",
                    description = stringResource(id = R.string.wind)
                )
            }
        }
    }
}

@Preview
@Composable
private fun PreviewCardContent() {
    CardContent(
        dailyWeatherModel = DailyWeatherModel(
            locations = listOf(
                DailyWeatherInfo(
                    "2022-12-15", 4.3, 31.7, 22.9,
                    1671062400, 27.3, 1011.2, 81.2, "rain", "overcast", 11.2, 27.12, "8.11", "18.11", 112.01
                )
            )
        ),
        page = 0, navigateToDetails = {}, unitPosition = hr.dice.lukac11.weatherapp.enums.Unit.STANDARD
    )
}
