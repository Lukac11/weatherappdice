package hr.dice.lukac11.weatherapp.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Coordinates(
    @SerialName("lon")
    val longitude: Double? = null,
    @SerialName("lat")
    val latitude: Double? = null
)
