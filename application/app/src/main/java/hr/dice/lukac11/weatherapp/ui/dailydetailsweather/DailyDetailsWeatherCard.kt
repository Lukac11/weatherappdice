package hr.dice.lukac11.weatherapp.ui.dailydetailsweather

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import hr.dice.lukac11.weatherapp.model.DailyDetailsCard

/**
 * Shows information about temperature, weather description and weather icon on a card.
 */
@Composable
fun DailyDetailsWeatherCard(dailyDetailsCard: DailyDetailsCard) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = MaterialTheme.colorScheme.background),
        border = BorderStroke(width = 2.dp, color = MaterialTheme.colorScheme.onBackground),

    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(MaterialTheme.colorScheme.background)
                .padding(4.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Column(modifier = Modifier.background(MaterialTheme.colorScheme.background).padding(start = 10.dp), verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
                Text(text = dailyDetailsCard.temperature, fontSize = 36.sp, fontWeight = FontWeight.ExtraBold, color = MaterialTheme.colorScheme.onBackground)
                Text(text = dailyDetailsCard.description, fontSize = 14.sp, fontWeight = FontWeight.Normal, color = MaterialTheme.colorScheme.onBackground)
            }
            AsyncImage(
                model = dailyDetailsCard.iconUrl,
                contentDescription = null,
                modifier = Modifier
                    .size(80.dp)
                    .padding(8.dp)

            )
        }
    }
}

@Composable
@Preview
private fun PreviewDailyDetailsWeather() {
    DailyDetailsWeatherCard(DailyDetailsCard("10.5", "kiša", "rain"))
}
