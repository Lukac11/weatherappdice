package hr.dice.lukac11.weatherapp.data

import hr.dice.lukac11.weatherapp.enums.RetrofitApiInstance
import hr.dice.lukac11.weatherapp.model.DailyWeatherModel
import hr.dice.lukac11.weatherapp.network.VisualCrossingWeatherApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Gets data from VisualCrossingWeatherApi service.
 *
 * @param visualCrossingWeatherApiService is used to access methods from VisualCrossingWeatherApiService interface
 */
class DailyWeatherRepository(private val visualCrossingWeatherApiService: VisualCrossingWeatherApiService) {

    suspend fun getDailyWeatherData(lat: Double, lon: Double, unit: String, language: String): DailyWeatherModel =
        withContext(Dispatchers.IO) {
            visualCrossingWeatherApiService.getDailyWeather("$lat,$lon", unit, language, RetrofitApiInstance.VISUAL_CROSSING_API.apiKey)
        }
}
