package hr.dice.lukac11.weatherapp.ui.currentweatherscreen

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.ramcosta.composedestinations.annotation.Destination
import hr.dice.lukac11.weatherapp.R
import hr.dice.lukac11.weatherapp.enums.Unit
import hr.dice.lukac11.weatherapp.model.WeatherModel
import hr.dice.lukac11.weatherapp.ui.errorscreen.ErrorContent
import hr.dice.lukac11.weatherapp.ui.loadingscreen.LoadingScreen
import hr.dice.lukac11.weatherapp.ui.nointernetconnectionscreen.NoInternetConnectionContent
import hr.dice.lukac11.weatherapp.ui.state.UiState
import org.koin.androidx.compose.getViewModel

/**
 * Screen shows a [CurrentWeatherContent] and state handling for data that is presented on this screen.
 * It uses data given from currentWeatherViewModel and puts it onto UI.
 */
@Destination
@ExperimentalAnimationApi
@Composable
fun CurrentWeatherScreen() {
    val viewModel = getViewModel<CurrentWeatherViewModel>()
    val uiState by viewModel.uiState.collectAsState()
    val unitPosition = viewModel.unitValue.collectAsState()

    AnimatedContent(targetState = uiState) { targetState ->
        when (targetState) {
            UiState.Loading -> LoadingScreen()
            UiState.InternetError -> NoInternetConnectionContent {
                viewModel.loadCityWeatherData()
            }
            UiState.Error -> ErrorContent {
                viewModel.loadCityWeatherData()
            }
            is UiState.Success<*> -> CurrentWeatherContent(
                targetState.data as WeatherModel, unitPosition.value
            )
        }
    }
}

/**
 * Shows information about current weather data on Screen.
 * @param weatherModel is used for presenting all information from api to screen
 */

@Composable
fun CurrentWeatherContent(weatherModel: WeatherModel, unitPosition: Unit) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Spacer(modifier = Modifier.weight(1F))
        AsyncImage(
            model = weatherModel.weather.first().iconUrl,
            contentDescription = null,
            modifier = Modifier
                .size(168.dp)
                .fillMaxWidth()
        )

        Text(
            color = MaterialTheme.colorScheme.onBackground,
            text = weatherModel.weather.first().description,
            modifier = Modifier
                .padding(top = 6.dp)
                .fillMaxWidth(),
            style = MaterialTheme.typography.titleLarge,
            textAlign = TextAlign.Center
        )

        Text(
            color = MaterialTheme.colorScheme.onBackground,
            text = "${weatherModel.mainWeatherInfo?.temp?.toInt()}${unitPosition.temperature}",
            modifier = Modifier
                .padding(top = 6.dp)
                .fillMaxWidth(),
            style = MaterialTheme.typography.displayLarge,
            textAlign = TextAlign.Center
        )
        Spacer(modifier = Modifier.weight(1F))
        Row(
            modifier = Modifier
                .padding(horizontal = 10.dp)
                .padding(bottom = 50.dp)
                .fillMaxWidth()
                .clip(shape = RoundedCornerShape(5.dp))
                .border(2.dp, MaterialTheme.colorScheme.onBackground, RectangleShape)
                .height(intrinsicSize = IntrinsicSize.Min),
            horizontalArrangement = Arrangement.SpaceEvenly,

        ) {
            RowElement(weatherParameter = "${weatherModel.mainWeatherInfo?.humidity?.toInt()} %", description = stringResource(R.string.humidity))
            Divider(
                modifier = Modifier
                    .fillMaxHeight()
                    .width(2.dp),
                color = MaterialTheme.colorScheme.onBackground
            )
            RowElement(weatherParameter = "${weatherModel.mainWeatherInfo?.pressure?.toInt()} ${unitPosition.pressure}", description = stringResource(R.string.pressure))

            Divider(
                modifier = Modifier
                    .fillMaxHeight()
                    .width(2.dp),
                color = MaterialTheme.colorScheme.onBackground
            )
            RowElement(weatherParameter = "${weatherModel.wind?.speed?.toInt()} ${unitPosition.speed}", description = stringResource(R.string.wind))
        }
        Spacer(modifier = Modifier.weight(1F))
    }
}

/**
 * Element that is used for showing humidity, pressure and wind speed inside [CurrentWeatherContent].
 * @param weatherParameter is used to present the value of weather element
 * @param description is used to present which element is shown
 */
@Composable
fun RowElement(weatherParameter: String, description: String) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {

        Text(
            color = MaterialTheme.colorScheme.onBackground,
            text = weatherParameter,
            style = MaterialTheme.typography.bodySmall,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(4.dp)
        )
        Text(
            color = MaterialTheme.colorScheme.onBackground,
            text = description,
            style = MaterialTheme.typography.bodySmall,
            modifier = Modifier.padding(4.dp)
        )
    }
}
