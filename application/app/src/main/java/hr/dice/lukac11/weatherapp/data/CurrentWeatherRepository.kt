package hr.dice.lukac11.weatherapp.data

import hr.dice.lukac11.weatherapp.enums.RetrofitApiInstance
import hr.dice.lukac11.weatherapp.model.WeatherModel
import hr.dice.lukac11.weatherapp.network.WeatherApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CurrentWeatherRepository(private val weatherApiService: WeatherApiService) {

    suspend fun getCityWeatherData(lat: Double, lon: Double, unit: String, language: String): WeatherModel =
        withContext(Dispatchers.IO) {
            weatherApiService.getCityWeather(lat, lon, RetrofitApiInstance.OPEN_WEATHER_API.apiKey, unit, language)
        }
}
