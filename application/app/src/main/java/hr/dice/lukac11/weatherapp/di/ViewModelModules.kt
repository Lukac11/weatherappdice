package hr.dice.lukac11.weatherapp.di

import hr.dice.lukac11.weatherapp.ui.StartLoaderViewModel
import hr.dice.lukac11.weatherapp.ui.currentweatherscreen.CurrentWeatherViewModel
import hr.dice.lukac11.weatherapp.ui.dailydetailsweather.DailyDetailsWeatherViewModel
import hr.dice.lukac11.weatherapp.ui.dailyweatherscreen.DailyWeatherViewModel
import hr.dice.lukac11.weatherapp.ui.nolocationsavedscreen.NoLocationSavedViewModel
import hr.dice.lukac11.weatherapp.ui.settingsscreen.SettingsViewModel
import hr.dice.lukac11.weatherapp.ui.topbar.TopBarViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { CurrentWeatherViewModel(get(), get(), get()) }
    viewModel { TopBarViewModel(get()) }
    viewModel { NoLocationSavedViewModel(get(), get()) }
    viewModel { DailyWeatherViewModel(get(), get(), get()) }
    viewModel {
        params ->
        DailyDetailsWeatherViewModel(get(), dailyWeatherInfo = params.get())
    }
    viewModel { SettingsViewModel(get()) }
    viewModel { StartLoaderViewModel(get()) }
}
