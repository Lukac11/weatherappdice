package hr.dice.lukac11.weatherapp

import android.app.LocaleManager
import android.os.Build
import android.os.Bundle
import android.os.LocaleList
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.core.os.LocaleListCompat
import com.ramcosta.composedestinations.DestinationsNavHost
import hr.dice.lukac11.weatherapp.ui.NavGraphs
import hr.dice.lukac11.weatherapp.ui.settingsscreen.SettingsViewModel
import hr.dice.lukac11.weatherapp.ui.theme.WeatherAppTheme
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalAnimationApi
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            WeatherAppTheme {
                val viewModel: SettingsViewModel by viewModel()
                val userSettings by viewModel.userSetting.collectAsState()
                val appLocale: LocaleListCompat = LocaleListCompat.forLanguageTags(userSettings.language.openWeatherApiValue)
                AppCompatDelegate.setApplicationLocales(appLocale)
                if (Build.VERSION.SDK_INT >= 33) {
                    applicationContext.getSystemService(
                        LocaleManager::class.java
                    ).applicationLocales = LocaleList.forLanguageTags(userSettings.language.openWeatherApiValue)
                }

                DestinationsNavHost(navGraph = NavGraphs.root)
            }
        }
    }
}
