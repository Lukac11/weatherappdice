package hr.dice.lukac11.weatherapp.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Weather(
    @SerialName("id")
    val id: Int? = null,
    @SerialName("main")
    val mainWeatherDescription: String? = null,
    @SerialName("description")
    val description: String,
    @SerialName("icon")
    val icon: String? = null,

    val iconUrl: String? = icon?.let { "http://openweathermap.org/img/wn/$it@2x.png" }
)
