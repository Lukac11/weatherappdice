package hr.dice.lukac11.weatherapp.data

import hr.dice.lukac11.weatherapp.database.LocationDao
import hr.dice.lukac11.weatherapp.model.LocationModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext

class LocationRepository(private val locationDao: LocationDao) {
    fun getActiveLocation(): Flow<LocationModel> {
        return locationDao.getActiveLocation().distinctUntilChanged().flowOn(Dispatchers.IO)
    }
    suspend fun insertLocation(lat: Double, lon: Double, isActive: Boolean) {
        withContext(Dispatchers.IO) {
            locationDao.insertLocation(LocationModel(latitude = lat, longitude = lon, cityName = "", isActive = isActive))
        }
    }
    suspend fun updateLocation(locationModel: LocationModel) =
        withContext(Dispatchers.IO) {
            locationDao.updateLocation(locationModel)
        }
    suspend fun checkIfLocationAvailable(): Boolean =
        withContext(Dispatchers.IO) {
            locationDao.checkIfLocationAvailable()
        }
}
