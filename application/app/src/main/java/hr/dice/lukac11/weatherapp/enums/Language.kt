package hr.dice.lukac11.weatherapp.enums

enum class Language(val openWeatherApiValue: String, val visualCrossingWeatherApiValue: String) {
    CROATIAN("hr", "sr"),
    ENGLISH("en", "en"),
    GERMAN("de", "de")
}
