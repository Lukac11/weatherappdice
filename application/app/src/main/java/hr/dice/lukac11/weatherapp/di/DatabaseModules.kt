package hr.dice.lukac11.weatherapp.di

import androidx.room.Room.databaseBuilder
import hr.dice.lukac11.weatherapp.Constants
import hr.dice.lukac11.weatherapp.database.AppDatabase
import org.koin.dsl.module

val databaseModule = module {
    single { databaseBuilder(get(), AppDatabase::class.java, Constants.DATABASE_NAME).fallbackToDestructiveMigration().build() }
    single { get<AppDatabase>().locationDao() }
}
