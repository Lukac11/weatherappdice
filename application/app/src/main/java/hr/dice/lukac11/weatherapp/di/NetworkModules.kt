package hr.dice.lukac11.weatherapp.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import hr.dice.lukac11.weatherapp.enums.RetrofitApiInstance
import hr.dice.lukac11.weatherapp.network.VisualCrossingWeatherApiService
import hr.dice.lukac11.weatherapp.network.WeatherApiService
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

@ExperimentalSerializationApi
val networkModule = module {
    val contentType = "application/json".toMediaType()
    single { get<Retrofit>(named(RetrofitApiInstance.OPEN_WEATHER_API)).create(WeatherApiService::class.java) }
    single(named(RetrofitApiInstance.OPEN_WEATHER_API)) {
        Retrofit
            .Builder()
            .baseUrl(RetrofitApiInstance.OPEN_WEATHER_API.baseUrl)
            .addConverterFactory(get<Json>().asConverterFactory(contentType))
            .build()
    }
    single { Json { ignoreUnknownKeys = true } }
    single { get<Retrofit>(named(RetrofitApiInstance.VISUAL_CROSSING_API)).create(VisualCrossingWeatherApiService::class.java) }
    single { get<Json>().asConverterFactory(contentType) }
    single(named(RetrofitApiInstance.VISUAL_CROSSING_API)) {
        Retrofit
            .Builder()
            .baseUrl(RetrofitApiInstance.VISUAL_CROSSING_API.baseUrl)
            .addConverterFactory(get())
            .build()
    }
}
