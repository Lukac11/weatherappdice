package hr.dice.lukac11.weatherapp.navigation
const val BOTTOM_NAVIGATION_CONTAINER_SCREEN_BASE_ROUTE = "bottom_nav_graph"
sealed class NavRoutes(val route: String) {
    object CurrentWeatherScreen : NavRoutes("currentWeather")
    object DailyWeatherScreen : NavRoutes("dailyWeather")
    object Settings : NavRoutes("settings")
    object StartLoaderScreen : NavRoutes("startLoader")
    object NoLocationSaved : NavRoutes("noLocationSaved")
    object MainBottomNavBarScreen : NavRoutes(BOTTOM_NAVIGATION_CONTAINER_SCREEN_BASE_ROUTE)
}
