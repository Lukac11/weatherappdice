package hr.dice.lukac11.weatherapp.database

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import hr.dice.lukac11.weatherapp.enums.Days
import hr.dice.lukac11.weatherapp.enums.Language
import hr.dice.lukac11.weatherapp.enums.Theme
import hr.dice.lukac11.weatherapp.enums.Unit
import hr.dice.lukac11.weatherapp.model.UserPreferences
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map

private const val DATASTORE_NAME = "Settings"
class PreferenceManager(context: Context) {
    private object PreferencesKeys {
        val THEME = stringPreferencesKey("theme")
        val LANGUAGE = stringPreferencesKey("language")
        val UNIT = stringPreferencesKey("unit")
        val NUMBEROFDAYS = stringPreferencesKey("numberofdays")
    }
    private val Context._dataStore: DataStore<Preferences> by preferencesDataStore(DATASTORE_NAME)
    private val dataStore = context._dataStore
    suspend fun updateUnits(units: Unit) {
        dataStore.edit { preferences ->
            preferences[PreferencesKeys.UNIT] = units.name
        }
    }

    suspend fun updateLanguage(language: Language) {
        dataStore.edit { preferences ->
            preferences[PreferencesKeys.LANGUAGE] = language.name
        }
    }

    suspend fun updateNumberOfDays(numberOfDays: Days) {
        dataStore.edit { preferences ->
            preferences[PreferencesKeys.NUMBEROFDAYS] = numberOfDays.name
        }
    }

    suspend fun updateTheme(theme: Theme) {
        dataStore.edit { preferences ->
            preferences[PreferencesKeys.THEME] = theme.name
        }
    }
    val preferencesFlow = dataStore.data.map { preferences ->
        val units = Unit.valueOf(
            preferences[PreferencesKeys.UNIT] ?: Unit.METRIC.name
        )
        val language = Language.valueOf(
            preferences[PreferencesKeys.LANGUAGE] ?: Language.ENGLISH.name
        )
        val numberOfDays = Days.valueOf(
            preferences[PreferencesKeys.NUMBEROFDAYS] ?: Days.THREE.name
        )
        val theme = Theme.valueOf(
            preferences[PreferencesKeys.THEME] ?: Theme.DEFAULT.name
        )
        UserPreferences(theme, language, units, numberOfDays)
    }.distinctUntilChanged()
}
