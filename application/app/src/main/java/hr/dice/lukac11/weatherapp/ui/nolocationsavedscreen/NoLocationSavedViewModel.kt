package hr.dice.lukac11.weatherapp.ui.nolocationsavedscreen

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.location.FusedLocationProviderClient
import hr.dice.lukac11.weatherapp.data.LocationRepository
import hr.dice.lukac11.weatherapp.ui.state.UiState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class NoLocationSavedViewModel(private val locationRepository: LocationRepository, private val fusedLocationProviderClient: FusedLocationProviderClient) : ViewModel() {
    private val _uiState = MutableStateFlow<UiState?>(null)
    val uiState: StateFlow<UiState?> = _uiState

    fun insertLocation(latitude: Double, longitude: Double, isActive: Boolean) {
        viewModelScope.launch {
            locationRepository.insertLocation(latitude, longitude, isActive)
        }
    }
    @SuppressLint("SuspiciousIndentation")
    fun getLocation() {
        _uiState.value = UiState.Loading
        fusedLocationProviderClient.getLocation(
            onLocationSuccess = { location ->
                _uiState.value = UiState.Success(location)
                insertLocation(location.latitude, location.longitude, true)
            },
            onLocationFailed = {
                _uiState.value = UiState.Error
            }
        )
    }
    fun updateUIStateToError() {
        _uiState.value = UiState.Error
    }
}
