package hr.dice.lukac11.weatherapp.ui.dailyweatherscreen

import hr.dice.lukac11.weatherapp.MainDispatcherRule
import hr.dice.lukac11.weatherapp.data.DailyWeatherRepository
import hr.dice.lukac11.weatherapp.data.LocationRepository
import hr.dice.lukac11.weatherapp.data.UserSettingsRepository
import hr.dice.lukac11.weatherapp.model.DailyWeatherInfo
import hr.dice.lukac11.weatherapp.model.DailyWeatherModel
import hr.dice.lukac11.weatherapp.ui.state.UiState
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import okio.IOException
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DailyWeatherViewModelTest {
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @MockK
    private lateinit var dailyWeatherRepository: DailyWeatherRepository
    @MockK
    private lateinit var locationRepository: LocationRepository

    @MockK
    private lateinit var settingsRepository: UserSettingsRepository

    private lateinit var dailyWeather: DailyWeatherModel

    private lateinit var viewModel: DailyWeatherViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        viewModel =
            DailyWeatherViewModel(dailyWeatherRepository, locationRepository, settingsRepository)
        dailyWeather = DailyWeatherModel(listOf(DailyWeatherInfo("", 0.0, 0.0, 0.0, 123, 0.0, 0.0, 0.0, "", "", 0.0, 0.0, "", "", 0.0)))
        Dispatchers.setMain(UnconfinedTestDispatcher())
    }
    @Test
    fun testUIStateLoading() {
        assertTrue(viewModel.uiState.value is UiState.Loading)
    }

    @Test
    fun testUIStateSuccess() {
        coEvery {
            dailyWeatherRepository.getDailyWeatherData(any(), any(), any(), any())
        } returns dailyWeather
        viewModel.loadCityWeatherData()
        assertTrue(viewModel.uiState.value is UiState.Success<*>)
    }
    @Test
    fun testUIStateNoInternetConnection() {
        coEvery {
            dailyWeatherRepository.getDailyWeatherData(any(), any(), any(), any())
        } throws IOException()
        viewModel.loadCityWeatherData()
        assertTrue(viewModel.uiState.value is UiState.InternetError)
    }
    @Test
    fun testUIStateError() {
        coEvery {
            dailyWeatherRepository.getDailyWeatherData(any(), any(), any(), any())
        } throws Exception()
        viewModel.loadCityWeatherData()
        assertTrue(viewModel.uiState.value is UiState.Error)
    }
    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}
