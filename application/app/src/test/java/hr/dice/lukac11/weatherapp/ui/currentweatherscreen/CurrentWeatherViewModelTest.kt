package hr.dice.lukac11.weatherapp.ui.currentweatherscreen

import hr.dice.lukac11.weatherapp.MainDispatcherRule
import hr.dice.lukac11.weatherapp.data.CurrentWeatherRepository
import hr.dice.lukac11.weatherapp.data.LocationRepository
import hr.dice.lukac11.weatherapp.data.UserSettingsRepository
import hr.dice.lukac11.weatherapp.model.MainWeatherInfo
import hr.dice.lukac11.weatherapp.model.Weather
import hr.dice.lukac11.weatherapp.model.WeatherModel
import hr.dice.lukac11.weatherapp.model.Wind
import hr.dice.lukac11.weatherapp.ui.state.UiState
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import okio.IOException
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CurrentWeatherViewModelTest {
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()
    @MockK
    private lateinit var currentWeatherRepository: CurrentWeatherRepository

    @MockK
    private lateinit var locationRepository: LocationRepository

    @MockK
    private lateinit var settingsRepository: UserSettingsRepository

    private lateinit var currentWeather: WeatherModel

    private lateinit var viewModel: CurrentWeatherViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        viewModel =
            CurrentWeatherViewModel(currentWeatherRepository, locationRepository, settingsRepository)
        currentWeather =
            WeatherModel(
                weather = listOf(Weather(0, "", "", "", "")),
                mainWeatherInfo = MainWeatherInfo(0.0, 0, 0, 0.0, 0.0),
                visibility = 0,
                wind = Wind(0.0, 0),
                id = 0,
                locationName = "",
                cod = 0
            )
        Dispatchers.setMain(UnconfinedTestDispatcher())
    }

    @Test
    fun testUIStateLoading() {
        assertTrue(viewModel.uiState.value is UiState.Loading)
    }

    @Test
    fun testUIStateSuccess() {
        coEvery {
            currentWeatherRepository.getCityWeatherData(any(), any(), any(), any())
        } returns currentWeather
        viewModel.loadCityWeatherData()
        assertTrue(viewModel.uiState.value is UiState.Success<*>)
    }
    @Test
    fun testUIStateNoInternetConnection() {
        coEvery {
            currentWeatherRepository.getCityWeatherData(any(), any(), any(), any())
        } throws IOException()
        viewModel.loadCityWeatherData()
        assertTrue(viewModel.uiState.value is UiState.InternetError)
    }
    @Test
    fun testUIStateError() {
        coEvery {
            currentWeatherRepository.getCityWeatherData(any(), any(), any(), any())
        } throws Exception()
        viewModel.loadCityWeatherData()
        assertTrue(viewModel.uiState.value is UiState.Error)
    }
    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}
