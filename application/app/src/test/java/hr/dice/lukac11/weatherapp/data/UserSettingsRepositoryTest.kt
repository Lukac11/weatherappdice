package hr.dice.lukac11.weatherapp.data

import hr.dice.lukac11.weatherapp.database.PreferenceManager
import hr.dice.lukac11.weatherapp.enums.Days
import hr.dice.lukac11.weatherapp.enums.Language
import hr.dice.lukac11.weatherapp.enums.Theme
import hr.dice.lukac11.weatherapp.enums.Unit
import hr.dice.lukac11.weatherapp.model.UserPreferences
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class UserSettingsRepositoryTest {
    @RelaxedMockK
    private lateinit var preferenceManager: PreferenceManager

    @MockK
    private lateinit var userPreferences: UserPreferences

    private lateinit var userSettingsRepository: UserSettingsRepository
    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        userSettingsRepository = UserSettingsRepository(preferenceManager)
    }

    @Test
    fun testGetPreferenceFlow() = runTest {
        coEvery {
            preferenceManager.preferencesFlow
        } returns flow {
            userPreferences
        }
        userSettingsRepository.getPreferenceFlow().collect {
            assertTrue(it == userPreferences)
        }
    }

    @Test
    fun testSaveUnit() = runTest {
        userSettingsRepository.saveUnit(Unit.IMPERIAL)
        coVerify {
            preferenceManager.updateUnits(any())
        }
    }

    @Test
    fun testSaveLanguage() = runTest {
        userSettingsRepository.saveLanguage(Language.CROATIAN)
        coVerify {
            preferenceManager.updateLanguage(any())
        }
    }

    @Test
    fun testSaveTheme() = runTest {
        userSettingsRepository.saveTheme(Theme.LIGHT)
        coVerify {
            preferenceManager.updateTheme(any())
        }
    }

    @Test
    fun testSaveNumberOfDays() = runTest {
        userSettingsRepository.saveNumberOfDays(Days.THREE)
        coVerify {
            preferenceManager.updateNumberOfDays(any())
        }
    }
}
