package hr.dice.lukac11.weatherapp.ui.dailydetailsweather

import app.cash.turbine.test
import hr.dice.lukac11.weatherapp.MainDispatcherRule
import hr.dice.lukac11.weatherapp.data.UserSettingsRepository
import hr.dice.lukac11.weatherapp.enums.Days
import hr.dice.lukac11.weatherapp.enums.Language
import hr.dice.lukac11.weatherapp.enums.Theme
import hr.dice.lukac11.weatherapp.enums.Unit
import hr.dice.lukac11.weatherapp.model.DailyWeatherInfo
import hr.dice.lukac11.weatherapp.model.UserPreferences
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DailyDetailsWeatherViewModelTest {
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()
    @MockK
    private lateinit var settingsRepository: UserSettingsRepository

    private var dailyWeatherInfo = DailyWeatherInfo("", 0.0, 0.0, 0.0, 1234, 0.0, 0.0, 0.0, "", "", 0.0, 0.0, "", "", 0.0)
    private lateinit var viewModel: DailyDetailsWeatherViewModel
    private var userPreferences =
        UserPreferences(Theme.LIGHT, Language.CROATIAN, Unit.STANDARD, Days.THREE)
    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        viewModel = DailyDetailsWeatherViewModel(settingsRepository, dailyWeatherInfo)

        Dispatchers.setMain(UnconfinedTestDispatcher())
    }
    @Test
    fun testSetDailyWeatherInfo() = runTest {
        coEvery {
            settingsRepository.getPreferenceFlow()
        } returns flow {
            emit(userPreferences)
        }
        viewModel.setDailyWeatherInfo()
        viewModel.dailyDetailsWeatherComponentList.test {
            val actual = awaitItem()
            assertTrue(actual.isNotEmpty())
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}
