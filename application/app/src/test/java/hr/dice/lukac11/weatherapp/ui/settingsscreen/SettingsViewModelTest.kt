package hr.dice.lukac11.weatherapp.ui.settingsscreen

import app.cash.turbine.test
import hr.dice.lukac11.weatherapp.MainDispatcherRule
import hr.dice.lukac11.weatherapp.data.UserSettingsRepository
import hr.dice.lukac11.weatherapp.enums.Days
import hr.dice.lukac11.weatherapp.enums.Language
import hr.dice.lukac11.weatherapp.enums.Theme
import hr.dice.lukac11.weatherapp.enums.Unit
import hr.dice.lukac11.weatherapp.model.UserPreferences
import io.mockk.MockKAnnotations
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.just
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SettingsViewModelTest {
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @MockK
    private lateinit var settingsRepository: UserSettingsRepository
    @MockK
    private lateinit var theme: Theme
    @MockK
    private lateinit var unit: hr.dice.lukac11.weatherapp.enums.Unit
    @MockK
    private lateinit var language: Language
    @MockK
    private lateinit var days: Days

    private lateinit var settingsViewModel: SettingsViewModel
    private var userPreferences = UserPreferences(Theme.LIGHT, Language.CROATIAN, Unit.STANDARD, Days.THREE)
    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        settingsViewModel = SettingsViewModel(settingsRepository)

        Dispatchers.setMain(UnconfinedTestDispatcher())
    }

    @Test
    fun testSaveTheme() {
        coEvery {
            settingsRepository.saveTheme(theme)
        } just Runs
        settingsViewModel.saveTheme(theme)
    }
    @Test
    fun testSaveUnit() {
        coEvery {
            settingsRepository.saveUnit(unit)
        } just Runs
        settingsViewModel.saveUnit(unit)
    }

    @Test
    fun testSaveLanguage() {
        coEvery {
            settingsRepository.saveLanguage(language)
        } just Runs
        settingsViewModel.saveLanguage(language)
    }

    @Test
    fun testSaveDays() {
        coEvery {
            settingsRepository.saveNumberOfDays(days)
        } just Runs
        settingsViewModel.saveNumberOfDays(days)
    }
    @Test
    fun testGetUserSettings() = runTest {
        coEvery {
            settingsRepository.getPreferenceFlow()
        } returns flow {
            emit(
                userPreferences
            )
        }
        val job = launch {
            settingsViewModel.userSetting.test {
                val actual = awaitItem()
                val expected = userPreferences
                assertEquals(expected, actual)
            }
        }
        job.join()
        job.cancel()
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}
