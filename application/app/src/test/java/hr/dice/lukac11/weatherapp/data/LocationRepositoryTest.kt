package hr.dice.lukac11.weatherapp.data

import hr.dice.lukac11.weatherapp.database.LocationDao
import hr.dice.lukac11.weatherapp.model.LocationModel
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.just
import io.mockk.mockk
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertTrue
import org.junit.Test

class LocationRepositoryTest {
    private val dao = mockk<LocationDao>()
    private val repo = LocationRepository(dao)
    private val locationModel = LocationModel(0, 0.0, 0.0, "London", true)

    @Test
    fun testGetActiveLocation() = runTest {
        coEvery {
            dao.getActiveLocation()
        } returns flow {
            locationModel
        }
        repo.getActiveLocation().collect {
            assertTrue(locationModel == it)
        }
    }

    @Test
    fun testInsertLocation() = runTest {
        val lat: Double = 0.0
        val lon: Double = 0.0
        val isActive: Boolean = true
        coEvery {
            dao.insertLocation(locationModel)
        } just Runs

        repo.insertLocation(lat, lon, isActive)

        coVerify {
            dao.insertLocation(locationModel)
        }
    }

    @Test
    fun testUpdateLocation() = runTest {
        coEvery {
            dao.updateLocation(locationModel)
        } just Runs
        repo.updateLocation(locationModel)
        coVerify {
            dao.updateLocation(locationModel)
        }
    }

    @Test
    fun testCheckIfLocationAvailable() = runTest {
        coEvery {
            dao.checkIfLocationAvailable()
        } returns true
        val expected = true
        assertTrue(expected == dao.checkIfLocationAvailable())
    }
}
