package hr.dice.lukac11.weatherapp.data

import hr.dice.lukac11.weatherapp.model.MainWeatherInfo
import hr.dice.lukac11.weatherapp.model.Weather
import hr.dice.lukac11.weatherapp.model.WeatherModel
import hr.dice.lukac11.weatherapp.model.Wind
import hr.dice.lukac11.weatherapp.network.WeatherApiService
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertThrows
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class CurrentWeatherRepositoryTest {
    @MockK
    lateinit var currentWeatherApiService: WeatherApiService

    lateinit var weatherModel: WeatherModel

    lateinit var currentWeatherRepository: CurrentWeatherRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        currentWeatherRepository = CurrentWeatherRepository(currentWeatherApiService)
        weatherModel = WeatherModel(
            weather = listOf(Weather(0, "", "", "", "")),
            mainWeatherInfo = MainWeatherInfo(0.0, 0, 0, 0.0, 0.0),
            visibility = 0,
            wind = Wind(0.0, 0),
            id = 0,
            locationName = "",
            cod = 0
        )
    }

    @Test
    fun testGetCityWeatherData() = runTest {
        coEvery {
            currentWeatherApiService.getCityWeather(any(), any(), any(), any(), any())
        } returns weatherModel
        val response = currentWeatherRepository.getCityWeatherData(1.1, 1.1, "metric", "en")
        assertTrue(response == currentWeatherApiService.getCityWeather(1.1, 1.1, "", "metric", "en"))
    }

    @Test
    fun testGetCityWeatherDataReturnsException() = runTest {
        coEvery {
            currentWeatherApiService.getCityWeather(any(), any(), any(), any(), any())
        } throws Exception()
        assertThrows(Exception::class.java) {
            runTest {
                currentWeatherRepository.getCityWeatherData(1.1, 1.1, "metric", "en")
            }
        }
    }
}
