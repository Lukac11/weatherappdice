package hr.dice.lukac11.weatherapp.model

import org.junit.Assert.assertThrows
import org.junit.Assert.assertTrue
import org.junit.Test

class DailyWeatherInfoKtTest {
    @Test
    fun `input 0 returns N`() {
        val result = getNewWindDirection(0.0)
        assertTrue(result == "N")
    }
    @Test
    fun `input 337,6 returns N`() {
        val result = getNewWindDirection(337.6)
        assertTrue(result == "N")
    }
    @Test
    fun `input 22,5 returns N`() {
        val result = getNewWindDirection(22.4)
        assertTrue(result == "N")
    }
    @Test
    fun `input 22,6 returns NE`() {
        val result = getNewWindDirection(22.6)
        assertTrue(result == "NE")
    }
    @Test
    fun `input 45 returns NE`() {
        val result = getNewWindDirection(45.0)
        assertTrue(result == "NE")
    }
    @Test
    fun `input 67,5 returns NE`() {
        val result = getNewWindDirection(67.5)
        assertTrue(result == "NE")
    }
    @Test
    fun `input 67,6 returns E`() {
        val result = getNewWindDirection(67.6)
        assertTrue(result == "E")
    }
    @Test
    fun `input 90 returns E`() {
        val result = getNewWindDirection(90.0)
        assertTrue(result == "E")
    }
    @Test
    fun `input 112,5 returns E`() {
        val result = getNewWindDirection(112.5)
        assertTrue(result == "E")
    }
    @Test
    fun `input 112,6 returns SE`() {
        val result = getNewWindDirection(112.6)
        assertTrue(result == "SE")
    }
    @Test
    fun `input 150 returns SE`() {
        val result = getNewWindDirection(150.0)
        assertTrue(result == "SE")
    }
    @Test
    fun `input 157,5 returns SE`() {
        val result = getNewWindDirection(157.5)
        assertTrue(result == "SE")
    }
    @Test
    fun `input 157,6 returns S`() {
        val result = getNewWindDirection(157.6)
        assertTrue(result == "S")
    }
    @Test
    fun `input 180 returns S`() {
        val result = getNewWindDirection(180.0)
        assertTrue(result == "S")
    }
    @Test
    fun `input 202,5 returns S`() {
        val result = getNewWindDirection(202.5)
        assertTrue(result == "S")
    }
    @Test
    fun `input 202,6 returns SW`() {
        val result = getNewWindDirection(202.6)
        assertTrue(result == "SW")
    }
    @Test
    fun `input 225 returns SW`() {
        val result = getNewWindDirection(225.0)
        assertTrue(result == "SW")
    }
    @Test
    fun `input 247,5 returns SW`() {
        val result = getNewWindDirection(247.5)
        assertTrue(result == "SW")
    }
    @Test
    fun `input 247,6 returns W`() {
        val result = getNewWindDirection(247.6)
        assertTrue(result == "W")
    }
    @Test
    fun `input 270 returns W`() {
        val result = getNewWindDirection(270.0)
        assertTrue(result == "W")
    }
    @Test
    fun `input 292,5 returns W`() {
        val result = getNewWindDirection(292.5)
        assertTrue(result == "W")
    }
    @Test
    fun `input 292,6 returns NW`() {
        val result = getNewWindDirection(292.6)
        assertTrue(result == "NW")
    }
    @Test
    fun `input 300 returns NW`() {
        val result = getNewWindDirection(300.0)
        assertTrue(result == "NW")
    }
    @Test
    fun `input 337,5 returns NW`() {
        val result = getNewWindDirection(337.5)
        assertTrue(result == "NW")
    }
    @Test
    fun `input negative returns N`() {
        assertThrows(IllegalArgumentException::class.java) { getNewWindDirection(-2.1) }
    }

    @Test
    fun `input bigger than 360 returns N`() {
        assertThrows(IllegalArgumentException::class.java) { getNewWindDirection(500.1) }
    }
}
