package hr.dice.lukac11.weatherapp.data

import hr.dice.lukac11.weatherapp.model.DailyWeatherModel
import hr.dice.lukac11.weatherapp.network.VisualCrossingWeatherApiService
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertThrows
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class DailyWeatherRepositoryTest {
    @MockK
    lateinit var visualCrossingWeatherApiService: VisualCrossingWeatherApiService

    @MockK
    lateinit var dailyWeatherModel: DailyWeatherModel

    lateinit var dailyWeatherRepository: DailyWeatherRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        dailyWeatherRepository = DailyWeatherRepository(visualCrossingWeatherApiService)
    }

    @Test
    fun testGetDailyWeatherData() = runTest {
        coEvery {
            visualCrossingWeatherApiService.getDailyWeather(any(), any(), any(), any())
        } returns dailyWeatherModel
        val response = dailyWeatherRepository.getDailyWeatherData(1.1, 1.1, "metric", "en")
        assertTrue(response == visualCrossingWeatherApiService.getDailyWeather("1.1,1.1", "metric", "en", ""))
    }

    @Test
    fun testGetDailyWeatherDataThrowsException() = runTest {
        coEvery {
            visualCrossingWeatherApiService.getDailyWeather(any(), any(), any(), any())
        } throws Exception()
        assertThrows(Exception::class.java) {
            runTest {
                dailyWeatherRepository.getDailyWeatherData(1.1, 1.1, "metric", "en")
            }
        }
    }
}
